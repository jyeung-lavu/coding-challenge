## Background
Some times you'll find yourself working with strings that contain the representation of some sort of data structure. In this exercise you will be asked to parse a string into a given data structure.

## Problem
Complete a function that takes a string as an input and it returns a collection of objects.

## Notes
* The input string format is a `,` comma-delimited list of "objects".
* The object string is a `|` piped-delimited.
* The first item in the object string represents the User ID.
* The second item in the object string represents the Class ID.

**Sample Input**
```javascript
const str = '5|||,6|15||,3|||||,9|20||';
```
**Sample Output**
```javascript
[
  { id: 5, classId: null },
  { id: 6, classId: 15 },
  { id: 3, classId: null },
  { id: 9, classId: 20 }
]
```